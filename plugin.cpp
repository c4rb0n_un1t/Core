#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this),
	m_habitsTracker(),
	m_userTasks(),
	m_impl(new HabitsTracker(this, m_habitsTracker, m_repeatUserTasks))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IHabitsTracker), m_impl}
	},
	{
		{INTERFACE(IHabitsTrackerDataExtention), m_habitsTracker},
		{INTERFACE(IUserTaskDataExtention), m_userTasks},
		{INTERFACE(IUserTaskRepeatDataExtention), m_repeatUserTasks},
		{INTERFACE(IUserTaskDateDataExtention), m_dateUserTasks},
	},
	{}
	);
}

void Plugin::onReady()
{
	m_impl->init();
}
