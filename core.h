#pragma once

#include <QtCore>

#include "../../../Application/Interfaces/icore.h"
#include "../../Common/Plugin/iplugin.h"
#include "../../Common/Plugin/plugin_base.h"

#include "simplelinker.h"

//! addtogroup CorePlugin_imp
//! {
class Core : public QObject, public PluginBase, public ICore, public IApplication
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.CorePlugin" FILE "PluginMeta.json")
	Q_INTERFACES(
	        IPlugin
	        ICore
	        IApplication)

public:
	Core();
	virtual ~Core() override;

	// ICore interface
public:
	virtual void coreInit(quint32 corePluginUID, QObject* appObject, QWeakPointer<IApplication> app) override;
	virtual bool coreFini() override;

	// PluginBase interface
public:
	virtual void onReady() override;

	// IApplication interface
public:
	QWidget *getParentWidget() override;
	const QVector<IPluginHandlerPtr> &getPlugins() override;
	IPluginHandlerPtr makePluginHandler(const QString &path) override;
	QStringList getCommandLineArguments() override;
	quint32 askUser(const QString& question, const QVariantList& options) override;
signals:
	void onUserAnswered(quint32 askId, quint16 optionIndex);

private:
	QWeakPointer<IApplication> m_app;
	QSharedPointer<SimpleLinker> m_linker;
};
//! }

